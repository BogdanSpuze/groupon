import { PlaywrightTestConfig, devices } from "@playwright/test";
import dotenv from "dotenv";

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
dotenv.config();

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const config: PlaywrightTestConfig = {
  testDir: "./e2e",
  globalSetup: require.resolve("./e2e/web/steps/GlobalSetup"),
  globalTeardown: require.resolve("./e2e/web/steps/GlobalTeardown"),
  /* Maximum time one test can run for. */
  timeout: 60 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000,
  },
  /* Run tests in files in parallel */
  fullyParallel: false,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: process.env.CI ? 1 : undefined,
  /* Opt out of parallel tests on CI. */
  workers: process.env.E2E_DEBUG ? 1 : 4,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: process.env.CI
    ? "line"
    : [["html", { outputFolder: "./e2e/test-results/", open: "never" }], ["list"]],
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0,
    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: "retain-on-failure",
    /* Run browser in a headful mode for debugging */
    /*
    launchOptions: {
      headless: process.env.E2E_DEBUG ? false : true,
      slowMo: process.env.E2E_DEBUG ? 500 : undefined,
    },*/
    // Tell all tests to load signed-in state from 'storageState.json'.
    // storageState: "./e2e/temp/sessionState.json",
  },

  /* Configure projects for major browsers */
  projects: [
    {
      name: "chromium",
      use: {
        ...devices["Desktop Chrome"],
        browserName: "chromium",
        launchOptions: {
          headless: process.env.E2E_DEBUG ? false : true,
          slowMo: process.env.E2E_DEBUG ? 500 : undefined,
          args: [
            "--disable-popup-blocking",
            "--use-fake-ui-for-media-stream",
            "--use-fake-device-for-media-stream",
            "--start-maximized",
          ],
        },
        contextOptions: {
          permissions: ["notifications", "camera", "microphone"],
        },
      },
    },

    {
      name: "firefox",
      use: {
        ...devices["Desktop Firefox"],
        browserName: "firefox",
        launchOptions: {
          headless: process.env.E2E_DEBUG ? false : true,
          slowMo: process.env.E2E_DEBUG ? 500 : undefined,
          firefoxUserPrefs: {
            "permissions.default.microphone": 1,
            "permissions.default.camera": 1,
            "media.navigator.streams.fake": true,
          },
        },
      },
    },

    {
      name: "webkit",
      use: {
        ...devices["Desktop Safari"],
      },
    },

    /* Test against mobile viewports. */
    // {
    //   name: 'Mobile Chrome',
    //   use: {
    //     ...devices['Pixel 5'],
    //   },
    // },
    // {
    //   name: 'Mobile Safari',
    //   use: {
    //     ...devices['iPhone 12'],
    //   },
    // },

    /* Test against branded browsers. */
    // {
    //   name: 'Microsoft Edge',
    //   use: {
    //     channel: 'msedge',
    //   },
    // },
    // {
    //   name: 'Google Chrome',
    //   use: {
    //     channel: 'chrome',
    //   },
    // },
  ],

  /* Folder for test artifacts such as screenshots, videos, traces, etc. */
  outputDir: "./e2e/test-artifacts/",

  /* Run your local dev server before starting the tests */
  /*webServer: {
    command: `CHAT_ORIGIN=${
      process.env.CHAT_ORIGIN || "https://service-staging.giosg.com"
    } npm run start`,
    port: 5000,
    reuseExistingServer: !process.env.CI,
  },
  */
};

export default config;
