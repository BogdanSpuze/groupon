import * as path from "path";
import { Browser, BrowserContext, Page, expect } from "@playwright/test";
import { TopBar } from "../pages/modules/TopBar";
import { SearchResults } from "../pages/SearchResults";
import { Home } from "../pages/Home";

export class ProductSteps {
  private readonly browser: Browser;
  private browserContext: BrowserContext;
  private page: Page;
  private topBarComponent: TopBar;
  private searchResultsPage: SearchResults;
  private homePage: Home;
  
  public constructor(browser: Browser) {
    this.browser = browser;
  }

  public async initialize() {
    this.browserContext = await this.browser.newContext();
    this.page = await this.browserContext.newPage();

    this.searchResultsPage = new SearchResults(this.page);
    this.homePage = new Home(this.page);
    this.topBarComponent = new TopBar(this.page);
  }

  public async closesBrowser() {
    await this.browserContext.close();
  }

  public async searchesFor(value: string){
    await this.homePage.visit();
    await this.topBarComponent.search(value);
  }

  public async validatesMultipleProductsAreDisplayed(){
    await this.searchResultsPage.waitSearchResultsAreDisplayed();
    let numberOfDisplayedProducts = await this.searchResultsPage.getNumberOfDisplayedResults();
    expect(numberOfDisplayedProducts > 1).toBe(true);
  }

  public async validatesNoProductIsDisplayed(){
    let numberOfDisplayedProducts = await this.searchResultsPage.getNumberOfDisplayedResults();
    expect(numberOfDisplayedProducts).toBe(0);
    
  }

  public async validatesNoResultsTextIsDisplayed(){
    //this text should be taken from the translation file of the product, depending on the language your site is displayed on.
    let noResultsText = "Your search returned no results.";
    expect(await this.searchResultsPage.isStringPresentOnPage(noResultsText)).toBe(true);
  }

  public async validatesSpecifficProductIsDisplayed(name: string, price: string){
    await this.searchResultsPage.waitSearchResultsAreDisplayed();
    expect(await this.searchResultsPage.isSpecifficProductDisplayed(name, price)).toBe(true);
  }

}
