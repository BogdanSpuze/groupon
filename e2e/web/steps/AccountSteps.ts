import * as path from "path";
import { Browser, BrowserContext, Page, expect } from "@playwright/test";
import { AccountCreate } from "../pages/AccountCreate";
import { AccountLogin } from "../pages/AccountLogin";

export class AccountSteps {
  private readonly browser: Browser;
  private browserContext: BrowserContext;
  private page: Page;
  private accountCreatePage: AccountCreate;
  private accountLoginPage: AccountLogin;
  
  public constructor(browser: Browser) {
    this.browser = browser;
  }

  public async initialize() {
    this.browserContext = await this.browser.newContext();
    this.page = await this.browserContext.newPage();

    this.accountCreatePage = new AccountCreate(this.page);
    this.accountLoginPage = new AccountLogin(this.page);
  }

  public async closesBrowser() {
    await this.browserContext.close();
  }

  public async triesToLogin(userEmail: string, userPassword: string){
    await this.accountLoginPage.visitLogin();
    await this.accountLoginPage.fillEmailAndPassword(userEmail, userPassword);
    await this.accountLoginPage.clickSignIn();
  }

  public async validatesLoginFail(){
    expect(await this.accountLoginPage.isSignInVisible()).toBe(true);
  }

  public async validatesLoginSuccess(){
    expect(await this.accountLoginPage.isSignInVisible()).toBe(false);
  }

  public async logsIn(userEmail: string, userPassword: string){
    await this.triesToLogin(userEmail, userPassword);
    await this.accountLoginPage.waitSignedInSuccessfully();
  }

  public async logsout() {
    await this.accountLoginPage.visitLogout();
  }

  public async triesToCreateANewAccount(firstName: string, lastName: string, email: string, password: string, confirmPassword: string){
    await this.accountCreatePage.visit();
    await this.accountCreatePage.fillInPersonalInformation(firstName, lastName);
    await this.accountCreatePage.fillInSignInInformation(email, password, confirmPassword);
    await this.accountCreatePage.clickCreateAccount();
  }

  public async createsNewAccount(firstName: string, lastName: string, email: string, password: string){
    await this.triesToCreateANewAccount(firstName, lastName, email, password, password);
    await this.accountCreatePage.waitAccountCreatedSuccessfully();
  }

  public async validatesAccountCreationFail(){
    expect(await this.accountCreatePage.isCreateAccountButtonVisible()).toBe(true);
  }

  public async validatesAccountCreationSuccess(){
    expect(await this.accountCreatePage.isCreateAccountButtonVisible()).toBe(false);
  }

}
