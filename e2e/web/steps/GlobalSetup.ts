import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '..', '..', 'test-data', 'env', `${process.env.ENVIRONMENT}.env`) });

async function globalSetup() {
}

export default globalSetup;
