import { type Page, expect } from "@playwright/test";

export class TopBar  {
  private readonly page: Page;

  private readonly searchInputId = "#search";
 
  public constructor(page: Page) {
    this.page = page;
  }

  public async search(value: string) {
    await this.page.locator(this.searchInputId).fill(value);
    await this.page.keyboard.press('Enter');
  }

}
