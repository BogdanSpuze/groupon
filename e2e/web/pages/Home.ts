import { type Page, expect } from "@playwright/test";

export class Home {
  private readonly page: Page;

  private readonly baseUrl = `${process.env.BASE_URL}`;
  
  private readonly homeMainClass = ".home-main";


  public constructor(page: Page) {
    this.page = page;
  }

  public async visit() {
    await this.page.goto(this.baseUrl);
    //normally I would edit the source of the software under test and add a speciffic data-testId to one of the classes and identify that, 
    //in this case I cannot so I have to do an ugly workaround
    await this.page.locator(this.homeMainClass).first().waitFor({state: 'visible'});
  }

}
