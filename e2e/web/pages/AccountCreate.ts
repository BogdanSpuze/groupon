import { type Page, expect } from "@playwright/test";

export class AccountCreate {
  private readonly page: Page;

  private readonly createUrl = `${process.env.BASE_URL}/customer/account/create`;

  private readonly firstNameInputId = "#firstname";
  private readonly lastNameInputId = "#lastname";
  private readonly emailInputId = "#email_address";
  private readonly passwordInputId = "#password";
  private readonly confirmPasswordInputId = "#password-confirmation";
  private readonly createAccountButtonTitle = "Create an Account";


  public constructor(page: Page) {
    this.page = page;
  }

  public async visit() {
    await this.page.goto(this.createUrl);
  }

  public async fillInPersonalInformation(firstName: string, lastName: string) {
    if( firstName != null && firstName !== "" ){
      await this.page.locator(this.firstNameInputId).fill(firstName); 
    }

    if( lastName != null && lastName !== "" ){
      await this.page.locator(this.lastNameInputId).fill(lastName); 
    }
  }

  public async fillInSignInInformation(email: string, password: string, confirmationPassword: string) {
    if( email != null && email !== "" ){
      await this.page.locator(this.emailInputId).fill(email); 
    }

    if( password != null && password !== "" ){
      await this.page.locator(this.passwordInputId).fill(password); 
    }

    if( confirmationPassword != null && confirmationPassword !== "" ){
      await this.page.locator(this.confirmPasswordInputId).fill(confirmationPassword); 
    }
  }

  public async clickCreateAccount(){
    await this.page.getByTitle(this.createAccountButtonTitle).click();
  }

  public async waitAccountCreatedSuccessfully(){
    await this.page.getByTitle(this.createAccountButtonTitle).waitFor({state: "hidden"});
  }

  public async isCreateAccountButtonVisible(): Promise<boolean> {
    return await this.page.getByTitle(this.createAccountButtonTitle).isVisible();
  }
}
