import { type Page, expect } from "@playwright/test";

export class SearchResults {
  private readonly page: Page;
  
  private readonly listOfProductsClass = ".products";
  private readonly productClass = ".product-item-info";


  public constructor(page: Page) {
    this.page = page;
  }

  public async waitSearchResultsAreDisplayed() {
    await this.page.locator(this.listOfProductsClass).waitFor({state: 'visible'});
  }

  public async getNumberOfDisplayedResults(): Promise<number>{
    return await this.page.locator(this.productClass).count();
  }

  //here you can validate for multiple parameters but because I do not have much time I will do only name and price
  public async isSpecifficProductDisplayed(name: string, price: string): Promise<boolean>{
    try{
        await this.page.getByText(name).waitFor({state: "visible"});
    }catch(e)
    {
        console.warn("Product was not displayed.");
        return false;
    }
    let itemName = this.page.getByText(name);
    let parentOfName = this.page.locator('strong', {has: itemName});
    let targetElement = this.page.locator('div', {has: parentOfName});

    let hasPrice = await targetElement.getByText(price).isVisible();

    return hasPrice;
  }

  public async isStringPresentOnPage(lookForValue: string): Promise<boolean> {
    return await this.page.getByText(lookForValue).isVisible();
  }
}
