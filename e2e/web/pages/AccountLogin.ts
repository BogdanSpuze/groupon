import { type Page, expect } from "@playwright/test";

export class AccountLogin {
  private readonly page: Page;
  private readonly loginUrl = `${process.env.BASE_URL}/customer/account/login`;
  private readonly logoutUrl = `${process.env.BASE_URL}/customer/account/logout`;

  private readonly emailInputId = "#email";
  private readonly passwordInputId = "#pass";
  private readonly signInButtonId = "#send2";

  // the texts used into testing should be taken from the translation file
  // another module should load the needed texts and we should take them from there
  private readonly youAreSignedOutText = "You are signed out";

  public constructor(page: Page) {
    this.page = page;
  }

  public async visitLogin() {
    await this.page.goto(this.loginUrl);
    await this.page.locator(this.signInButtonId).waitFor({state: "visible"});
  }

  public async visitLogout() {
    await this.page.goto(this.logoutUrl);
    await this.page.getByText(this.youAreSignedOutText).waitFor({state: "visible"});
  }


  public async fillEmailAndPassword(email: string, password: string){
    if( email != null && email !== "" ){
      await this.page.locator(this.emailInputId).fill(email); 
    }

    if( password != null && password !== "" ){
      await this.page.locator(this.passwordInputId).fill(password); 
    }
  }

  public async clickSignIn() {
    await this.page.locator(this.signInButtonId).filter({has: this.page.getByRole('button')}).click();
  }

  public async waitSignedInSuccessfully() {
    await this.page.locator(this.signInButtonId).waitFor({state: "hidden"});
  }

  public async isSignInVisible(): Promise<boolean>{
    return await this.page.locator(this.signInButtonId).isVisible();
  }
}
