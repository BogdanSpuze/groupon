# Things to remember/know:

-

# How to run tests:

- Note: more scripts to run with different parameters can be added to package.json file

ENVIRONMENT=PROD npx playwright test

# How to check the results:

- After the tests are run the command is displayed in console.
- Note: Depending on how you run the tests, you will have access to more or less details in the test-results.

`npx playwright show-report e2e/test-results`

# Project Structure:

- playwright.config.ts (found in the root folder) - to configure playwright
- e2e (found in root folder) - is where the whole e2e project lives
  - features - each big feature has a file with its description in natural language (tried to use Gherkin code)
  - test-data - here reside the media files used by the tests
  - web - the code of the tests
    - steps - feature files are calling steps files methods, here is where the 'brains' of the test resides
    - pages - interface class between the web page and the test code, dummy description of webpage with elements identifiers and actions. No logic is implemented here. Steps methods are calling the pages classes to handle UI.
