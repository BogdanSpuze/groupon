import { test } from "@playwright/test";
import { ProductSteps } from "../web/steps/ProductSteps";

test.describe.configure({ mode: "serial" });

test.describe("As an user I want to be able to search my product on the webpage", () => {
  let user: ProductSteps;

  test.beforeAll(async ({ browser }) => {
    user = new ProductSteps(browser);
    await user.initialize();
  });

  test.afterAll(async () => {
    await user.closesBrowser();
  });

  test("Multiple products can be displayed on the page", async ({ browser }) => {
    let searchParameter = "yoga";

    //When
    await user.searchesFor(searchParameter);

    //Then
    await user.validatesMultipleProductsAreDisplayed();
  });

  test("When there is no result, speciffic message will be displayed", async ({ browser }) => {
    let searchParameter = "lyon";

    //When
    await user.searchesFor(searchParameter);

    //Then
    await user.validatesNoProductIsDisplayed();
    //And
    await user.validatesNoResultsTextIsDisplayed();
  });

  test("Speciffic product is displayed after search", async ({ browser }) => {
    
    let productName = "Pierce Gym Short";
    let productPrice = "27.00";
    //When
    await user.searchesFor(productName);

    //Then
    await user.validatesMultipleProductsAreDisplayed();
    //And
    await user.validatesSpecifficProductIsDisplayed(productName, productPrice);
  });

});
