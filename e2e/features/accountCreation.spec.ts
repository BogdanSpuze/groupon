import { test } from "@playwright/test";
import { AccountSteps } from "../web/steps/AccountSteps";

test.describe.configure({ mode: "serial" });

test.describe("As an user I want to be able to create an account", () => {
  let user: AccountSteps;
  let userLoggedIn;

  test.beforeAll(async ({ browser }) => {
    user = new AccountSteps(browser);
    await user.initialize();
  });

  test.afterAll(async () => {
    await user.closesBrowser();
  });

  test.afterEach(async ({ browser }) => {
    if(userLoggedIn == true){
      await user.logsout();
    }
  });

  test("Account creation fails if email already used", async ({ browser }) => {
    
    let userEmail = `${process.env.DEMO_USER}`;
    let userPassword = `PasswordIsValid123`;
    let userFirstName= "Bogdan";
    let userLastName = "Spuze";
    userLoggedIn = false;

    //When
    await user.triesToCreateANewAccount(userFirstName, userLastName, userEmail, userPassword, userPassword);

    //Then
    await user.validatesAccountCreationFail();
  });


  test("Account creation fails if passwords do not match", async ({ browser }) => {
    
    let min = 0;
    let max = 99999999;
    let random = Math.floor(Math.random() * (max - min + 1) + min);
    let userEmail = `user_${random}@yopmail.com`; 
    let userPassword = `PasswordIsValid123`;
    let userPasswordConfirmation = `AnotherDifferentPasswordIsValid123`;
    let userFirstName= "Bogdan";
    let userLastName = "Spuze";
    userLoggedIn = false;

    //When
    await user.triesToCreateANewAccount(userFirstName, userLastName, userEmail, userPassword, userPasswordConfirmation);

    //Then
    await user.validatesAccountCreationFail();
  });

  test("Account can be created", async ({ browser }) => {
    let min = 0;
    let max = 99999999;
    let random = Math.floor(Math.random() * (max - min + 1) + min);
    let userEmail = `user_${random}@yopmail.com`; 
    // yes I know I could have done it better
    // in an ideal world I can access the database and wipe all created accounts after one run
    // also I know I should check if user email is not already used, also checking in the DB or in the error I receive from the UI
    // here for simplicity I assume user will be always unique.
    let userPassword = `SomePassword@123`;
    let userFirstName= "Bogdan";
    let userLastName = "Spuze";
    userLoggedIn = true;

    //When
    await user.createsNewAccount(userFirstName, userLastName, userEmail, userPassword);

    //Then
    await user.validatesAccountCreationSuccess();

  });
});
