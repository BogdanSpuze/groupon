import { test } from "@playwright/test";
import { AccountSteps } from "../web/steps/AccountSteps";

test.describe.configure({ mode: "serial" });

test.describe("As an user I want to be able to login in the website", () => {
  let user: AccountSteps;

  test.beforeAll(async ({ browser }) => {
    user = new AccountSteps(browser);
    await user.initialize();
  });

  test.afterAll(async () => {
    await user.closesBrowser();
  });

  test.afterEach(async ({ browser }) => {
    await user.logsout();
  });

  test("Login is successfull", async ({ browser }) => {
    
    let userEmail = `${process.env.DEMO_USER}`;
    let userPassword = `${process.env.DEMO_USER_PASSWORD}`;

    //When
    await user.logsIn(userEmail, userPassword);

    //Then
    await user.validatesLoginSuccess();
  });

  test("Login fails if used does not exist", async ({ browser }) => {
    
    let userEmail = `some_random_user@whatever_email.com`;
    let userPassword = `PasswordIsValid123`;

    //When
    await user.triesToLogin(userEmail, userPassword);

    //Then
    await user.validatesLoginFail();
  });
});
